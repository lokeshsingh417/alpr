from django.contrib import admin
from .models import Image,ANPRLPDetails,ANPRMaster,ANPRVehicleRegisterationDetail
# Register your models here.
admin.site.register([Image,ANPRLPDetails,ANPRMaster,ANPRVehicleRegisterationDetail])